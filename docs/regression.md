# numdjs Regression
A collection of least squares fitting methods.
- linear
- exponential
- logarithmic
- power
- polynomial

[Regression Examples](https://lucidlylogicole.gitlab.io/numdjs/examples/regression.html)

## Usage

    const {regression} = require('src/regression.js')
    let x = [2,4,6,8]
    let y = [3,7,5,10]
    let result = regression.linear(x,y)
 
Returns

    {
      coefficients: { b0: 1.5, b1: 0.95 },
      equation: 'y = 1.5 + 0.95x',
      r2: 0.67,
      standard_error: 2.09,
      predict: [Function: predict],
      points: [Function: points]
    }

## API Definition

**regresion** - the main regression object

**.precision** - (integer) the number of significant digits for the output (default is 4)

> to change the precision: `regression.precision=3`

**.linear(x, y)** - fits the input data to a straight line

> y = b<sub>0</sub> + b<sub>1</sub>x

**.exponential(x, y)** fits the input data to a exponential curve

> y = b<sub>0</sub>e<sup>b<sub>1</sub>x</sup>

**.logarithmic(x, y)** - fits the input data to a logarithmic curve

> y = b<sub>0</sub> + b<sub>1</sub>ln(x)

**.power(x, y)** - fits the input data to a power law curve

> y = b<sub>0</sub>x<sup>b<sub>1</sub></sup>

**.polynomial(x, y, order)** - fits the input data to a polynomial curve
- `x` (array) - array of x data
- `y` (array) - array of y data
- `order` (integer) - the order of the polynomial used to fit the data

> y = b<sub>0</sub> + b<sub>1</sub>x + ... b<sub>n</sub>x<sup>n</sup>

Example:

    regression.polynomial(x, y, 3)

## Output Result
All functions return a result in the format:

    {
      coefficients: { b0: 1.5, b1: 0.95 },
      equation: 'y = 1.5 + 0.95x',
      r2: 0.67,
      standard_error: 2.09,
      predict: [Function: predict],
      points: [Function: points]
    }

- `coefficients`: the coefficients of the equation
- `equation`: string display of the equation
- `r2`: the coefficient of determination (<i>r</i><sup>2</sup>)
- `standard_error`: the standard error / standard deviation
- `predict(x)`: function that will calculate predicted y values utilizing the equation
- `points()`: function will return an array containing the predicted y data using the x as inputs


## References
- Numerical Methods for Engineers (5th Edition) by Steven C. Chapra and Raymond P.Canale
- [Tom Alexander's regression.js](https://github.com/Tom-Alexander/regression-js)
    - The Gaussian Elimination method used in the polynomial regression comes from this module
    - The polynomial regression is a modified version of the version in Tom's library.
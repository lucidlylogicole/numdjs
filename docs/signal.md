# numdjs Signal
Downsampling and filtering functions for data signal processing.

[Signal Examples](https://lucidlylogicole.gitlab.io/numdjs/examples/signal.html)

## Usage

    const {signal} = require('src/signal.js')
    let x = [2,4,6,8..1000]
    let y = [3,7,5,10..2032]
    let [xd,yd] = signal.decimate(x,y,10)
    let [xm,ym] = signal.mean(x,y,10)
    let ym = signal.movingAverage(y,10)
    let yf = signal.lowPassFilter(y,10)
 

## API Definition

**signal** - the main signal processing object

**.decimate(x,y,m,offset)** - downsample by keeping only every m<sup>th</sup> sample of x and y. Returns the new x and y arrays.
- `x` - array of `x` values
- `y` - array of `y` values (length of y should match the length of x)
- `m` - keep every m<sup>th</sup> point
    - Example: if `m=10`, then every 10th point will be kept
- `offset` (optional) the off

> `let [xd,yd] = signal.decimate(x,y,10)`

**.mean(x,y,m)** - downsample by averaging every `m` number of y values and return at the middle x value.  Returns the new x and y arrays.
- `x` - array of `x` values
- `y` - array of `y` values (length of y should match the length of x)
- `m` - the number of points to average
- if m is odd, then the closest x point to the middle is used

> `let [xm,ym] = signal.mean(x,y,10)`

**.movingAverage(y,m)** - average every m<sup>th</sup> number of y values.  Returns the new y array.
- `y` - array of `y` values
- `m` - the number of points to average
- also called moving mean or rolling mean
- the average is taken with an equal number of points on each side of the central value.  If `m=5`, then 2 points before the current point and 2 points after the current point are averaged with the current point.

> `let ym = signal.movingAverage(y,10)`

**.lowPassFilter(x, y, w0)** - passes signals with a frequency lower than a selected cutoff frequency (`w0`) and attenuates signals with frequencies higher than the cutoff frequency
- `x` - array of `x` values
- `y` - array of `y` values (length of y should match the length of x)
- `w0` - cutoff frequency
    - also equal to `1/RC`

> `let yf = signal.lowPassFilter(x,y,1)`
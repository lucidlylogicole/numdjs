# numdjs Base
Typical basic math functions

## API Definition

**random(n, d)** - generate a random number from `0` to `n`
- `n` - max random number
- `d` - (optional) decimal places.  default is 0

> `random(6)  // random numbers from 0 to 6`

**range(start, stop, step)** - generate an array 
- `start` - starting number
- `stop` - number to stop at (this is not included in the resulting array)
- `step` - step between each number

> `range(0,10,2)  // [0,2,4,6,8]`

**range(start, stop)** - generate an array of integers
- `start` - starting number
- `stop` - number to stop at (this is not included in the resulting array)

> `range(1,5)  // [1,2,3,4]`

**range(stop)** - generate an array of integers starting at 0
- `stop` - number to stop at (this is not included in the resulting array)

> `range(4)  // [0,1,2,3]`

**round(n, d)** - round the number `n`, to `d` decimal places
- `n` - number to round
- `d` - (optional) decimal places.  default is 0

> `round(5/3,2)  // rounds to 1.67`

**isNumber(n)** - returns true or false if `n` is a number
- `n` - value to check

> `isNumber('dog') // false`

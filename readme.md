# numdjs
A NUmerical MethoDs library for JavaScript that works with NodeJS and in Web Browsers.

## Features
- works with NodeJS and Web Browsers
- no dependencies
- minimizes intermodule dependencies

## Docs
- [Base](docs/base.md) - basic math functions
- [Regression](docs/regression.md) - least squares fitting methods
    - [Regression Examples](https://lucidlylogicole.gitlab.io/numdjs/examples/regression.html)
- [Signal](docs/signal.md) - data signal processing methods
    - [Signal Examples](https://lucidlylogicole.gitlab.io/numdjs/examples/signal.html)

## License
[MIT](LICENSE)

## Development Strategy
- this will start out to include methods that interest me
- will probably incorporate methods from [numeric](https://github.com/sloisel/numeric) and  [numjs](https://github.com/numjs/numjs)

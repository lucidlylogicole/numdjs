// numdjs Signal Processing of Data    (c) 2023 lucidlylogicole    Released under the MIT License

//---o:
const signal = {
    VERSION:'1.1.0',

//---Downsampling
    decimate: function(x,y,m,offset) {
        if (offset === undefined) {
            offset = 1
        }
        if (m === undefined && typeof(y) == 'number') {
            // downsample 2d array
            let datad = []
            for (let i=offset-1; i < x.length; i+=y) {
                datad.push(x[i])
            }
            return datad
            
        } else {
            // Downsample x and y
            let xd = []
            let yd = []
            for (let i=offset-1; i < x.length; i+=m) {
                xd.push(x[i])
                yd.push(y[i])
            }
            return [xd,yd]
        }
        
    },

    mean: function(x,y,m) {
        let xd = []
        let yd = []
        let cum_sum_y = 0
        x_off = m-Math.round(m/2)
        for (let i=0; i < y.length; i+=1) {
            if (i >0 && (i+1) % m == 0) {
                ya = cum_sum_y/m
                xd.push(x[i-x_off])       // align with data points
                yd.push(ya)
                cum_sum_y = 0
            }
            cum_sum_y += y[i]
        }
        
        return [xd,yd]
    },

    movingAverage: function(y,m) {
        let yd = []
        let x_prev = Math.round((m-1)/2)
        let x_after = m-x_prev
        for (let i=0; i < y.length; i+=1) {
            let ist = Math.max(i-x_prev,0)
            let ifn = Math.min(i+x_after,y.length)
            let v = 0
            for (let ix=ist; ix<ifn; ix++) {
                v += y[ix]
            }
            yd.push(v/(ifn-ist))
        }
        
        return yd
        
    },

//---Filters
    lowPassFilter: function(x,y,w0) {
        let yf = []
        let RC = 1/w0
        for (let i=0; i < x.length; i++) {
            if (i == 0) {
                yf.push(y[i])
            } else {
                let td = x[i]-x[i-1]
                yf.push(td*y[i] + RC*yf[i-1]/(td + RC))
            }
        }
        return yf
    },

}


//---
//---Module Exports
if (typeof(module) !== 'undefined') {
    module.exports = {signal}
}
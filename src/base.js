// numdjs Basic Math Functions  v1.0.0   (c) 2023 lucidlylogicole    Released under the MIT License

function random(n,d) {
    // Random numbers
    if (n === undefined) {n=1}
    return round(Math.random()*n,d)
}

function range(start,stop,step) {
    // Create array of numbers
    if (step === undefined) {step=1}
    if (stop === undefined) {stop=start; start=0}
    var n = Math.floor((stop-start)/step)
    return Array.apply(null, Array(n)).map(function (_, i) {return i*step + start})
}

function round(n,d) {
    // round number to decimal places
    if (d === undefined){d=0}
    let factor = 10**d
    return Math.round(n*factor)/factor
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n)
}

//---
//---Module Exports
if (typeof(module) !== 'undefined') {
    module.exports = {random,range,round,isNumber}
}
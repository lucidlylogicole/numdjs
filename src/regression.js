// numdjs Regression    (c) 2023 lucidlylogicole    Released under the MIT License

//---o:
const regression = {
    VERSION:'1.1.2',
    precision: 4,
    
    //---Utilities
    round:function(d) {
        let factor = 10**regression.precision
        return Math.round(d*factor)/factor
    },

    analyze:function(x,y, sum_y, predict, order) {
        if (order == undefined){order=1}
        let n = x.length
        // r squared - coefficient of determination
        let ss_res = 0      // Sum of Squares Residual
        let ss_tot = 0      // Sum of Squared Total
        let y_mean = sum_y/n
        let points=[]
        for (let i=0; i<n; i++) {
            let xp = x[i]
            yi = y[i]
            fi = predict(xp)
            ss_res += (yi-fi)**2
            ss_tot += (yi-y_mean)**2
            points.push(fi)
        }
        let r2 = 1-ss_res/ss_tot
        
        // Standard Error / Standard Deviation
        let se = (ss_res/(n-(order+1)))**0.5
        
        return {r2:r2, SSres:ss_res, SStot:ss_tot, standard_error:se, points:points}
    },

    gaussianElimination: function(input, order) {
        // source for this function: https://github.com/Tom-Alexander/regression-js
        let matrix = input
        let n = input.length - 1
        let coefficients = [order]
        
        for (let i = 0; i < n; i++) {
            let maxrow = i
            for (let j = i + 1; j < n; j++) {
                if (Math.abs(matrix[i][j]) > Math.abs(matrix[i][maxrow])) {
                    maxrow = j
                }
            }
            
            for (let k = i; k < n + 1; k++) {
                let tmp = matrix[k][i]
                matrix[k][i] = matrix[k][maxrow]
                matrix[k][maxrow] = tmp
            }
            
            for (let j = i + 1; j < n; j++) {
                for (let k = n; k >= i; k--) {
                    matrix[k][j] -= (matrix[k][i] * matrix[i][j]) / matrix[i][i]
                }
            }
        }
        
        for (let j = n - 1; j >= 0; j--) {
            let total = 0;
            for (let k = j + 1; k < n; k++) {
                total += matrix[k][j] * coefficients[k]
            }
            
            coefficients[j] = (matrix[n][j] - total) / matrix[j][j]
        }
        
        return coefficients
    },

    //---
    //---Regression Functions
    linear: function(x,y, options) {
        
        // Get Sums
        let n = x.length
        let sum_x=0,sum_y=0,sum_x2=0,sum_xy=0
        for (let i=0; i<n; i++) {
            sum_x+=x[i]
            sum_y+=y[i]
            sum_x2+=x[i]**2
            sum_xy+=x[i]*y[i]
        }

        // Regression Coefficients
        let b1 = (n*sum_xy - sum_x*sum_y) / (n*sum_x2 - sum_x**2)
        let b0 = regression.round(sum_y/n - b1*sum_x/n)
        b1 = regression.round(b1)

        // Prediction based on coefficients
        function predict(x) {
            return regression.round(x*b1+b0)
        }

        let analysis = regression.analyze(x,y,sum_y, predict)
        
        return {
            coefficients:{b0:b0,b1:b1},
            equation:`y = ${b0} + ${b1}x`,
            r2:regression.round(analysis.r2),
            standard_error:regression.round(analysis.standard_error),
            predict:predict,
            points:function(){return analysis.points},
        }

    },

    exponential: function(x,y, options) {
        // Get Sums
        let n = x.length
        let sum_x=0, sum_y=0, sum_x2=0, sum_xy=0, sum_y_r=0
        for (let i=0; i<n; i++) {
            sum_x+=x[i]
            sum_y+=Math.log(y[i])
            sum_y_r+=y[i]
            sum_x2+=x[i]**2
            sum_xy+=x[i]*Math.log(y[i])
        }

        // Regression Coefficients
        let b1 = (n*sum_xy - sum_x*sum_y) / (n*sum_x2 - sum_x**2)
        let b0 = regression.round(sum_y/n - b1*sum_x/n)
        b0 = regression.round(Math.exp(b0))
        b1 = regression.round(b1)

        // Prediction based on coefficients
        function predict(x) {
            return regression.round(b0*Math.exp(x*b1))
        }

        let analysis = regression.analyze(x,y,sum_y_r,predict)
        
        return {
            coefficients:{b0:b0,b1:b1},
            equation:`y = ${b0}e^${b1}x`,
            r2:regression.round(analysis.r2),
            standard_error:regression.round(analysis.standard_error),
            predict:predict,
            points:function(){return analysis.points},
        }

    },

    logarithmic: function(x,y, options) {
        // Get Sums
        let n = x.length
        let sum_x=0, sum_y=0, sum_x2=0, sum_xy=0, sum_y_r=0
        for (let i=0; i<n; i++) {
            sum_x+=Math.log(x[i])
            sum_y+=y[i]
            sum_x2+=Math.log(x[i])**2
            sum_xy+=Math.log(x[i])*y[i]
        }

        // Regression Coefficients
        let b1 = (n*sum_xy - sum_x*sum_y) / (n*sum_x2 - sum_x**2)
        let b0 = regression.round(sum_y/n - b1*sum_x/n)
        b1 = regression.round(b1)

        // Prediction based on coefficients
        function predict(x) {
            return regression.round(b0 +b1*Math.log(x))
        }

        let analysis = regression.analyze(x,y,sum_y,predict)
        
        return {
            coefficients:{b0:b0,b1:b1},
            equation:`y = ${b0} + ${b1}ln(x)`,
            r2:regression.round(analysis.r2),
            standard_error:regression.round(analysis.standard_error),
            predict:predict,
            points:function(){return analysis.points},
        }

    },

    power: function(x,y, options) {
        // Get Sums
        let n = x.length
        let sum_x=0, sum_y=0, sum_x2=0, sum_xy=0, sum_y_r=0
        for (let i=0; i<n; i++) {
            sum_x+=Math.log10(x[i])
            sum_y+=Math.log10(y[i])
            sum_y_r+=y[i]
            sum_x2+=Math.log10(x[i])**2
            sum_xy+=Math.log10(x[i])*Math.log10(y[i])
        }

        // Regression Coefficients
        let b1 = (n*sum_xy - sum_x*sum_y) / (n*sum_x2 - sum_x**2)
        let b0 = regression.round(Math.pow(10,sum_y/n - b1*sum_x/n))
        b1 = regression.round(b1)

        // Prediction based on coefficients
        function predict(x) {
            return regression.round(b0*x**b1)
        }

        let analysis = regression.analyze(x,y,sum_y_r,predict)
        
        return {
            coefficients:{b0:b0,b1:b1},
            equation:`y = ${b0}x^${b1}`,
            r2:regression.round(analysis.r2),
            standard_error:regression.round(analysis.standard_error),
            predict:predict,
            points:function(){return analysis.points},
        }

    },


    polynomial: function(x,y,order, options) {
        // modified from this source: https://github.com/Tom-Alexander/regression-js
        let lhs = []
        let rhs = []
        let a = 0
        let b = 0
        let n = x.length
        let k = order + 1
        
        // Setup Matrix for Gaussian Elimination
        for (let i = 0; i < k; i++) {
            for (let l = 0; l < n; l++) {
                if (y[l] !== null) {
                    a += (x[l] ** i) * y[l]
                }
            }
          
            lhs.push(a)
            a = 0
            let c = []
            for (let j = 0; j < k; j++) {
                for (let l = 0; l < n; l++) {
                    if (y[l] !== null) {
                        b += x[l] ** (i + j)
                    }
                }
                c.push(b)
                b = 0
            }
            rhs.push(c)
        }
        rhs.push(lhs)
        
        let coefficients = regression.gaussianElimination(rhs, k).map(v => regression.round(v))
        
        function predict(x) {
            return coefficients.reduce((sum, coeff, power) => sum + (coeff * (x**power)), 0)
        }
        
        // calculate sum of y
        let sum_y = 0
        for (let i=0; i < n; i++) {
            sum_y += y[i]
        }
        
        let analysis = regression.analyze(x,y,sum_y,predict,order)
        
        // get equation and coefficients
        let eq = 'y = '
        let coeff = {}
        for (let i = 0; i<coefficients.length; i++) {
            coeff['b'+i] = coefficients[i]
            if (i > 1) {
                eq += ` + ${coefficients[i]}x^${i}`
            } else if (i === 1) {
                eq += ` + ${coefficients[i]}x`
            } else {
                eq += coefficients[i]
            }
        }
        
        return {
            coefficents:coeff,
            equation:eq,
            r2:regression.round(analysis.r2),
            standard_error:regression.round(analysis.standard_error),
            predict:predict,
            points:function(){return analysis.points},
        }

    },

    saturation_growth: function(x,y, options) {
        //---todo:
        return 'not implemented'
    },


}


//---
//---Module Exports
if (typeof(module) !== 'undefined') {
    module.exports = {regression}
}